$(document).ready(function () {
    const DELAY = 400

    const BR2 = 992
    const BR3 = 768
    const BR5 = 480


    $(window).resize(function () {

        otherGenres()
    })

    adaptiveMenu()
    headerFixed()
    otherGenres()

    function otherGenres() {

            if($('.popular-nav__item--other').length) {

                if ($(window).width() > BR2) {
                        $('.popular-nav__item--other').outerWidth($('.manga-row__col').width())


                }
                $('.popular-nav__item--other').click(function(e){

                    if($(e.target).hasClass('js-other') || $(e.target).hasClass('popular-nav__item--other')) {
                        $(this).toggleClass('active')
                    }

                    e.stopPropagation()

                })
                $(document).click(function(e){
                    $('.popular-nav__item--other.active').removeClass('active')
                })

            }

    }

    function adaptiveMenu() {
        if ($(window).width() < BR2) {
            $('.mob-menu').append($('.main-menu'))
            $('.mob-menu').append($('.search-wrapper'))
            $('body').click(function (e) {

                if ($(this).hasClass('active')) {

                    if (!$(e.target).closest('.mob-menu').length) {
                        $('.menu-close').click()
                    }
                }
            })
            $('.main-menu__item--has-children').click(function () {
                $(this).toggleClass('main-menu__item--active')
            })
        } else {
            $('.header-inner').append($('.main-menu'))
            $('.header-inner').append($('.search-wrapper'))
            $('.menu-close').click()
            $('body, .main-menu__item--has-children').off('click')
        }
    }

    $(".refresh").click(function () {
        var captcha = $(".captcha-img");
        var src = $(captcha).attr("src");
        if ((i = src.indexOf("?")) == -1) src += "?" + Math.random();
        else src = src.substring(0, i) + "?" + Math.random();
        $(captcha).attr("src", src);
    });


    $('.burger').click(function () {
        $('.mob-menu').css('display', 'block')
        setTimeout(function () {
            $('body, .mob-menu').addClass('active')
        }, 100)

    })
    $('.menu-close').click(function () {
        $('body, .mob-menu').removeClass('active')
        $('.main-menu__item--active').removeClass('main-menu__item--active')
        setTimeout(function () {
            $('.mob-menu').css('display', 'none')
        }, DELAY)

    })


    function headerFixed() {
        if ($(this).scrollTop() > 0) {
            $('.header').addClass('header--fixed')
        } else {
            $('.header').removeClass('header--fixed')
        }
    }


    $(window).scroll(function () {
        headerFixed()
    })



    $('.top-slider').slick({
        dots: true,
        arrows: true,
        slidesToShow: 1,
        infinite: true,
        fade: true,
        rows: 0,
        autoplay: true,
        autoplaySpeed: 4000,
        pauseOnHover: false,
        pauseOnDotsHover: false,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    centerMode: false
                }
            },
            {
                breakpoint: 992,
                settings: {
                    arrows: false
                }
            }
        ]
    })

    $('.hot-slider').slick({
        dots: true,
        arrows: true,
        slidesToShow: 6,
        slidesToScroll: 6,
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 5,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]

    })

    if ($(".genres-list").length) {
        if ($(window).width() >= BR2) {
            $(".genres-list").mCustomScrollbar({
                theme: "3d-thick-dark"
            })
        }
    }

    $('.subscribe-link').click(function(){
        let id = $(this).data('id')

        if(!$(this).hasClass('subscribed')) {
            $(this).addClass('subscribed')
            $(this).text('Отписаться')
        } else {
            $(this).removeClass('subscribed')
            $(this).text('Подписаться')
        }

        query = 'func=manage_subscription&id=' + id

        ajax(query, error, successEditLikes)

        return false
    })

    function successEditLikes(data) {
        console.log(data)
    }


    $('.blog-controls__btn').click(function () {
        let target = $(this).data('href')
        $('.blog-controls__btn').removeClass('blog-controls__btn--active')
        $(this).addClass('blog-controls__btn--active')
        $('.blog__wrapper').fadeOut(DELAY).promise().done(() => {
            $(target).fadeIn(DELAY)
        })
    })


    if ($('.authorEditor').length && $('.interpreterEditor').length) {
        let aEditor = textboxio.replace('.authorEditor')
        let iEditor = textboxio.replace('.interpreterEditor')

        let parentId = null

        $('.blog-area__publish-btn').click(function(){
            const id = $(this).closest('.blog__wrapper').attr('id')
            parentId = id
            let content
            if(id === 'authorArea') {
                content = aEditor.content.get()
            } else if(id === 'authorArea') {
                content = iEditor.content.get()
            }

            query = "func=add_article&book=" + $(this).data('id') + "&text=" + encodeURIComponent(content)

            ajax(query, error, successAddArticle)

            return false
        })

        function successAddArticle(data) {
            if(data) {
                data = data.r;
                data = JSON.parse(data)



                let html = getTemplateArticle(data.id, data.text, data.date, data.img_path)


                $('#' + parentId).find('.blog-area__form').after(html)

                const editors = textboxio.get('.editor')

                editors.forEach(function(ed){
                    ed.content.set('')
                })

                parentId = null

                alert('Статья успешно опубликована!')
            } else error()

        }

        function getTemplateArticle(id, text, date, img_path) {
            return `
            <article class="article blog-area__article" id="${id}">
                    <div class="article__top">
                    <div class="article__date">${date}</div>
                <div class="article__id">ID статьи: <strong>${id}</strong></div>
              
                    <div class="article-options js-opt-basic">
                        <a href="#" class="article-options__link article-options__link--edit" data-aid="${id}" title="Редактировать"><img src="${img_path}pencil.svg" alt=""></a>
                        <a href="#" class="article-options__link article-options__link--delete" data-aid="${id}" title="Удалить"><img src="${img_path}trash.svg" alt=""></a>
                        </div>
    
                        <div class="article-options js-opt-edit article-options--hidden">
                        <a href="#" class="article-options__link article-options__link--save" data-aid="${id}" title="Сохранить изменения"><img src="${img_path}save.svg" alt=""></a>
                        <a href="#" class="article-options__link article-options__link--cancel" data-aid="${id}" title="Отменить редактирование"><img src="${img_path}cancel.svg" alt=""></a>
                    </div>
                  
                </div>
                <div class="article__content" data-content="${id}">
                    ${text}
                </div>
            </article>`
        }

        $(document).on('click', '.article-options__link--edit', function () {
            let article = $(this).closest('.article')
            let ac = article.find('.article__content')

            ac.addClass('h-400')

            textboxio.replace('[data-content="' + ac.attr('data-content') + '"]')

            article.find('.js-opt-basic').addClass('article-options--hidden')
            article.find('.js-opt-edit').removeClass('article-options--hidden')

            return false
        })

        $(document).on('click', '.article-options__link--cancel', function () {
            let article = $(this).closest('.article')
            let ac = article.find('.article__content')

            ac.removeClass('h-400')


            let id = ac.attr('data-content')


            let activeEditor = getActiveEditor(id)

            activeEditor.restore()

            article.find('.js-opt-basic').removeClass('article-options--hidden')
            article.find('.js-opt-edit').addClass('article-options--hidden')

            return false
        })

        function getActiveEditor(id) {
            let editors = textboxio.get('.article__content')
//console.log(editors.filter(ed => $(ed.element()).closest('.article__content').attr('data-content') === id)[0])
            return editors.filter(ed => $(ed.element()).closest('.article__content').attr('data-content') == id)[0]
        }

        $(document).on('click', '.article-options__link--save', function () {
            let article = $(this).closest('.article')
            let ac = article.find('.article__content')

            ac.removeClass('h-400')


            let aid = $(this).attr('data-aid')


            let activeEditor = getActiveEditor(aid)

            let text = activeEditor.content.get()

            query = "func=edit&obj=article&name=text_" + aid + "&value=" + encodeURIComponent(text)
            ajax(query, error, successEditArticle)


            return false
        })

        function successEditArticle(data) {
            if (data["r"]) {
                data = data["r"]
                data = JSON.parse(data)

                let active = getActiveEditor(data.id)

                active.content.set(data.text)

                active.restore()

                $('#' + data.id).find('.js-opt-basic').removeClass('article-options--hidden')
                $('#' + data.id).find('.js-opt-edit').addClass('article-options--hidden')


            } else error()

        }


        let tmp_aid = null

        $(document).on('click', '.article-options__link--delete', function () {
            if (confirm(`Вы действительно хотите удалить статью с ID ${$(this).closest('.article').attr('id')}?`)) {
                tmp_aid = $(this).closest('article').attr('id')

                let query = "func=delete&obj=article&id=" + $(this).attr('data-aid')
                ajax(query, error, successDeleteArticle)
            }
            return false
        })

        function successDeleteArticle(data) {
            if (data["r"]) {
                $("#" + tmp_aid).fadeOut(DELAY, function () {
                    $("#" + tmp_aid).remove()
                    tmp_aid = null
                })
            } else error()

        }

    }

    let $chapterSlider = $('.chapter-slider')

    $chapterSlider.slick({
        arrows: false,
        rows: 0,
        speed: DELAY,
        fade: true,
        adaptiveHeight: true,
        cssEase: 'linear',
        infinite: false,
        lazyLoad: 'ondemand',
        slidesToShow: 1,
        slidesToScroll: 1
    })

    let activeSlide = 0

    $chapterSlider.on('afterChange', function(event, slick, currentSlide){
        activeSlide = currentSlide
        $('.chapter-nav__btn--number .select-value span').html(currentSlide + 1)

        // const $img = $chapterSlider.find('.slick-slide[data-slick-index="' + currentSlide + '"]').find('img')
        //
        // if($img.attr('data-src')) {
        //     $img.attr('src', $img.attr('data-src'))
        //     $img.removeAttr('data-src').fadeIn(DELAY)
        //
        //     $chapterSlider.slick('setPosition')
        // }
    })

    // $chapterSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
    //     const $img = $chapterSlider.find('.slick-slide[data-slick-index="' + nextSlide + '"]').find('img')
    //
    //     if($img.attr('data-src')) {
    //         $img.attr('src', $img.attr('data-src'))
    //         $img.removeAttr('data-src')
    //
    //         $chapterSlider.slick('setPosition')
    //     }
    // })

    function checkActiveSlide(flag = false) {
            if (activeSlide === 0 && (flag && flag === 'prev')) {
                if ($('.chapter-prevnext__link--prev').length) {
                    document.location.href = $('.chapter-prevnext__link--prev').attr('href')
                } else {
                    document.location.href = $('.breadcrumbs__item').eq(0).attr('href')
                }
            } else if(activeSlide === $chapterSlider.find('.slick-slide').length - 1 && (flag && flag === 'next')) {
                if($('.chapter-prevnext__link--next').length) {
                    document.location.href = $('.chapter-prevnext__link--next').attr('href')
                } else {
                    document.location.href = $('.breadcrumbs__item').eq(0).attr('href')
                }
            }
    }

    $('.chapter-nav__btn--prev').click(function () {
        $chapterSlider.slick('slickPrev')
        scrollToTop()



    })
        $('.chapter-slider img').click(function () {
            checkActiveSlide('next')
        })
    $('.chapter-nav__btn--next, .chapter-slider img').click(function () {
        $chapterSlider.slick('slickNext')
        scrollToTop()
    })

    $.fn.enterKey = function (fnc, code) {
        return this.each(function () {
            $(this).keydown(function (ev) {
                var keycode = (ev.keyCode ? ev.keyCode : ev.which);
                if (keycode == code) {
                    fnc.call(this, ev);
                }
            })
        })
    }

    if($('.chapter-slider').length) {

        function CustomSelect(options) {
            let elem = options.elem;
            elem.click(function(event) {
                console.log( event.target == elem[0])
                if (
                    event.target == elem[0] ||
                    $(event.target).closest('.select-value').length
                ) {
                    toggle();
                }
                else if ($(event.target).closest('.selected-item').length) {
                    setValue(
                        $(event.target).closest('.selected-item').html()
                    );
                }
            })

            let isOpen = false;

            function onDocumentClick(event) {
                console.log($(event.target).closest(elem).length)
                if (!$(event.target).closest(elem).length) close()
            }

            function setValue(html) {
                $('.select-value span').html(html)
                $chapterSlider.slick('slickGoTo', html - 1)
                scrollToTop()
                close()
            }

            function toggle() {
                console.log(isOpen)
                if (isOpen) close()
                else open()
            }

            function open() {
                elem.addClass('js-items-shown');
                $(document).click(function(e){
                    onDocumentClick(e)
                });
                isOpen = true;
            }

            function close() {
                elem.removeClass('js-items-shown');
                $(document).unbind('click', onDocumentClick);
                isOpen = false;
            }

        }

        new CustomSelect({
            elem: $('.number-select1')
        })
        new CustomSelect({
            elem: $('.number-select2')
        })

        $(document).enterKey(function () {
            checkActiveSlide('prev')
            $chapterSlider.slick('slickPrev')
            scrollToTop()
        }, 37)

        $(document).enterKey(function () {
            checkActiveSlide('next')
            $chapterSlider.slick('slickNext')
            scrollToTop()
        }, 39)

        $chapterSlider.contextmenu(function(){
            return false;
        });

    }



    function scrollToTop() {
        $("html, body").stop().animate({
            scrollTop: 0
        }, DELAY)
    }
    $("input[type='file']").each(function(){
        let $label = $("label[for='" + $(this).attr("id") + "']")
        $label.attr('data-text', $label.text())
    })
    $("input[type='file']").change(function() {
        let str = $(this).val()
        let i
        let $label = $("label[for='" + $(this).attr("id") + "']")
        if (str.lastIndexOf('\\')){
            i = str.lastIndexOf('\\')+1
        }
        else{
            i = str.lastIndexOf('/')+1
        }
        let filename = str.slice(i)
        if(!filename.length) {
            $label.text($label.data('text'))
        } else {
            $label.text(filename)
        }

    });


    $('input[name="avatar"]').change((e) => {
        if(e.target.files.length) {
            onFileUpload(e.target.files[0])
        }

    })


    let cropper = null

    function onFileUpload(file) {
        if (cropper) {
            cropper.destroy()
        }

        const reader = new FileReader()

        reader.onload = () => {
            let $img = $('.profile-img-wrapper__img').attr('src', reader.result)


            cropper = new Cropper($img[0], {
                aspectRatio: 1,
                background: false,
                viewMode: 3,
                autoCropArea: .7,
                movable: false,
                zoomable: false,
                rotatable: false,
                scalable: false,
                cropBoxResizable: false,
                minContainerWidth: 100,
                minContainerHeight: 100,
                crop: function (e) {
                    let imgData = cropper.getData()

                    for(let key in imgData) {
                        $('form[name="change_avatar"] input[name="' + key + '"]').val(Math.round(imgData[key]))
                    }



                }
            })
            $('.profile-img-form .form-group').removeClass('form-group--hidden')
        }

        reader.readAsDataURL(file)
    }

    $('.avatar-submit').click(function () {
        let imgurl = cropper.getCroppedCanvas().toDataURL()
        console.log(imgurl)
        return false
    })



    $('.bought .manga-block').click(function () {
        let html = $(this).find('.manga-hidden-info').html()
        if (html && html.length) {
            $('.modal-body').html(html)
            $('.manga-modal').addClass('modal-active')
        }


        return false
    })

    $('.profile-settings-link').click(function () {
        $('.settings-modal').addClass('modal-active')
        $('.settings-slider').slick('setPosition')
        return false
    })

    $(document).click(function (e) {
        if (!$(e.target).closest('.modal').length) {
            $('.modal').removeClass('modal-active')
        }
    })

    $('.modal-close, .modal-close-btn').click(function () {
        $(this).closest('.modal').removeClass('modal-active')
    })

    $('.modal-apply-btn').click(function () {
        let frameAvatarClass = $('.settings__view .account-img-block').attr('data-class')
        let frameCommentClass = $('.settings__frame').attr('data-class')

        let file = $('#cover')[0].files[0]

       let formData = new FormData()
        if(file) {
            formData.append('cover', file)
        }
        formData.append('func', 'edit_user_settings')
        formData.append('fields', "frame_avatar_class=>" + frameAvatarClass + ",frame_comment_class=>" + frameCommentClass)

        ajax(formData, error, successEditUserSettings, false)
    })

    $('.choose-link').click(function () {
        let cName = $(this).data('class')
        if ($(this).hasClass('choose-link--avatar')) {
            $('.settings__view .account-block__img-block').attr('data-class', cName)
        } else if ($(this).hasClass('choose-link--frame')) {
            $('.settings__frame').attr('data-class', cName)
        }
        return false
    })

    let settingsOpts = {
        arrows: true,
        rows: 0,
        slidesToShow: 5,
        prevArrow: '<button class="set-sl-btn settings-prev"></button>',
        nextArrow: '<button class="set-sl-btn settings-next"></button>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 380,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    }



    $('.avatar-slider').slick(settingsOpts)

    $('.frame-slider').slick(settingsOpts)




    $(document).on('click', '.comment-options__rating .rating-icon__img', function () {
        let $parent = $(this).closest('.rating__item')
        let $sibling = $parent.siblings()
        let $amt = $parent.find('.rating-amt')
        let $sibAmt = $sibling.find('.rating-amt')
        let num = +$amt.text()
        let sibNum = +$sibAmt.text()




        if ($sibAmt.hasClass('active')) {
            $sibAmt.removeClass('active')
            $sibAmt.text(--sibNum)

            $amt.addClass('active')
            $amt.text(++num)
        } else {
            if($amt.hasClass('active')) {
                $amt.removeClass('active')
                $amt.text(--num)
            } else {
                $amt.addClass('active')
                $amt.text(++num)
            }
        }
        let queryStr = ''
        if($amt.hasClass('rating-amt--likes')) {
            queryStr = '&fields=likes=>' + $amt.text() + ',dislikes=>' + $sibAmt.text() + '&like_or_dis=comments_likes'
        } else {
            queryStr = '&fields=likes=>' + $sibAmt.text() + ',dislikes=>' + $amt.text() + '&like_or_dis=comments_dislikes'
        }
            let commentId = $(this).closest('.comment').attr('id').substr("comment_".length)

        query = "func=update_likes&id=" + commentId + queryStr

        ajax(query, error, successEditLikes)
    })

    let tmp_id = 0
    let tmp_comment = null

    $(document).on("click", ".comment-cancel", function () {
        commentCancel()
    })

    $(document).on("click", "#add_comment", function () {
        commentCancel()
        showFormComment()
    })

    $(document).on("click", ".comment-options__link--reply", function () {
        commentCancel()
        let parentId = $(this).closest(".comment").attr('id')

        $(".comment-form__title").text("Ответить")


        $(".comment-form").appendTo("#" + parentId)

        $(".parent-id").val(parentId.substr("comment_".length))

        showFormComment()

        return false
    })

    $(document).on("click", "#form_add_comment .comment-form__submit-btn", function (event) {
        if ($(".comment-form__text").val()) {
            let query
            let comment_id = $(".comment-id").val()
            let text_comment = $(".comment-form__text").val()
            if (comment_id != 0) {
                query = "func=edit&obj=comment&name=text_" + comment_id + "&value=" + encodeURIComponent(text_comment)
                ajax(query, error, successEditComment)
            } else {
                let parent_id = $(".parent-id").val()

                let chapter_id = null
                if($('#form_add_comment .chapter-id').length) {
                    chapter_id = $('.chapter-id').val()
                }
                let book_id = $(".book-id").val()

                query = "func=add_comment&parent_id=" + parent_id + '&book_id=' + book_id + '&chapter_id=' + chapter_id + '&text=' + encodeURIComponent(text_comment)
                ajax(query, error, successAddComment)
            }
        } else alert("Вы не ввели текст комментария!")
    })

    $(document).on("click", ".comment-options__link--edit", function () {
        commentCancel()
        let parentId = $(this).closest(".comment").attr('id')

        tmp_comment = $("#" + parentId).clone()

        $(".comment-form__title").text("Редактировать")

        $(".comment-id").val(parentId.substr("comment_".length))

        let temp = $("#" + parentId + " .comment-text").html()

        temp = temp.replace(/&lt/g, "<")
        temp = temp.replace(/&gt/g, ">")
        temp = temp.replace(/&amp/g, "&")


        $(".comment-form__text").val(temp)

        $("#" + parentId).replaceWith($(".comment-form"))

        showFormComment()

        return false
    })

    $(document).on("click", ".comment-options__link--delete", function (event) {
        commentCancel()
        if (confirm("Вы уверены, что хотите удалить комментарий?")) {
            let commentId = $(this).closest(".comment").attr('id').substring("comment_".length)
            tmp_id = commentId
            let query = "func=delete&obj=comment&id=" + commentId
            ajax(query, error, successDeleteComment)
        }
        return false
    })


    function error() {
        alert("Произошла ошибка! Попробуйте еще раз или обратитесь к администрации.")
    }

    function successAddComment(data) {
        data = data["r"]
        data = JSON.parse(data)
        let comment = getTemplateComment(data.id, data.frame_comment_class, data.frame_avatar_class, data.user_id, data.login, data.avatar, data.text, data.date)
        if (data.parent_id != 0) {

            $("#form_add_comment").appendTo(".comments-form")
            $(".comment-form").css("display", "block")
            $("#comment_" + data.parent_id).append(comment)
        } else $(".comments-list").append(comment)

        closeFormComment()
    }

    function successEditLikes(data) {
        console.log(data)
    }

    function successEditComment(data) {
        if (data["r"]) $(tmp_comment).find(".comment-text").eq(0).html(data["r"])
        if (data) {
            console.log(data)
            let form = $(".comment-form").clone()
            $(".comment-form").replaceWith($(tmp_comment))
            tmp_comment = null
            $(".comments-list").after(form)

        } else error()
        closeFormComment()
    }

    function successDeleteComment(data) {
        if (data["r"]) {
            $("#comment_" + tmp_id).fadeOut(DELAY, function () {
                $("#comment_" + tmp_id).remove()
                $(".comments-count").text($(".comment").length)
                tmp_id = 0
            })
        } else error()
    }

    function successEditUserSettings(data) {
        if (data["r"]) {
            data = JSON.parse(data["r"])
            let $topFrame = $('.fs-section__account .account-img-block')
            if($topFrame.attr('data-class') != data.frame_avatar_class) {
                $topFrame.attr('data-class', data.frame_avatar_class)
            }
            if(data.account_cover) {
                $('.user-cover').attr('src', data.account_cover)
            }
            $('#cover').val('')
            $('#cover').trigger('change')

            alert('Настройки успешно сохранены!')
        } else error()
    }

    function getTemplateComment(id, comment_class, avatar_class, user_id, login, avatar, text, date) {
        let str = `
<div class="comment comments-list__comment" id="comment_${id}" data-class="${comment_class}">
\t<div class="comment-row">
\t\t<div class="comment-avatar comment-row__avatar" data-class="${avatar_class}">
\t\t\t<img src="${avatar}" alt="" class="comment-avatar__img">
\t\t</div>
\t\t<div class="comment-info">
\t\t\t<div class="comment-name comment-info__name">${login}</div>
\t\t\t<div class="comment-text">${text}</div>

\t\t\t<div class="comment-bottom comment-row__bottom">
\t\t\t\t<div class="comment-date comment-bottom__date">${date}</div>
\t\t\t\t<div class="comment-options">
          <div class="rating comment-options__show-rating">
\t\t\t\t\t\t<div class="rating__item">
\t\t\t\t\t\t\t<div class="rating-icon">
\t\t\t\t\t\t\t\t<img src="/images/tech_img/rate2.svg" alt="" class="rating-icon__img">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class="rating-amt rating-amt--likes ">0</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class="rating__item">
\t\t\t\t\t\t\t<div class="rating-icon">
\t\t\t\t\t\t\t\t<img src="/images/tech_img/rate3.svg" alt="" class="rating-icon__img">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class="rating-amt rating-amt--dislikes ">0</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<a href="#" class="comment-options__link comment-options__link--reply">Ответить</a>
\t\t\t\t\t<a href="#" class="comment-options__link comment-options__link--edit">Редактировать</a>
\t\t\t\t\t<a href="#" class="comment-options__link comment-options__link--delete">Удалить</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t</div>
\t</div>
        `
        return str
    }

    function showFormComment() {
        $(".comment-form__text").focus()
        $(".comment-cancel").removeClass("comment-cancel--hidden")
    }

    function commentCancel() {
        if (tmp_comment) {
            successEditComment(true)
        }
        closeFormComment()
    }

    function closeFormComment() {
        $(".comments-list").after($('.comment-form'))
        $(".comment-cancel").addClass('comment-cancel--hidden')
        $(".comment-form__title").text("Добавить комментарий")
        $(".parent-id").val(0)
        $(".comment-form__text").val("")
        $(".comment-id").val(0)
        $(".comments-count").text($(".comment").length)
    }

    function ajax(data, func_error, func_success, content_type = true) {

        let options = {
            url: "/api.php",
            type: "POST",
            data: (data),
            processData: false,

            error: func_error,
            success: function (result) {
                console.log(result)
                result = $.parseJSON(result)
                func_success(result)
            }
        }

        if(!content_type) {
            options.contentType = false
        }

        $.ajax(options)
    }

})


